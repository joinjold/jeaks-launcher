
	/////////////////////////////////
	   ///////////HOWTO///////////
	   ///////////////////////////

1.	Move the `htdocs\launcher` folder to your `htdocs` or `wwww` 

2.	Go to `http://yourdomain.com/launcher` and install the database. Other links if installed locally: `http://127.0.0.1/launcher`

2.	Edit `SIMPLE_CONFIG.cs` from source folder [ ~\launcher\SIMPLE_CONFIG.cs ]

4.	Compile with Visual Studio [ 2015 or later if possible ]

5. 	`launcher.exe` WILL BE CREATED IN ~\launcher\bin\Release\launcher.exe

bug reports: https://bitbucket.org/paulcosma97/jeaks-launcher/issues/new
I'm still working on this project. If you have any suggestions or bugs to report please contact me.


